// Core
import { FC } from 'react';
import { useTranslation } from 'next-i18next';

export const Footer: FC = () => {
    const { t } = useTranslation('common');

    return (
        <footer className = 'footer mt-30'>
            <div className = 'container'>
                <div className = 'row'>
                    <div className = 'col-lg-12'>
                        <div className = 'footer_bottm'>
                            <div className = 'row'>
                                <div className = 'col-md-6'>
                                    <ul className = 'fotb_left'>
                                        <li>
                                            <p>
                                            © 2021
                                                { ' ' }
                                                <strong>Lectrum LLC</strong>.{' '}
                                                {t('copyright')}
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
};
