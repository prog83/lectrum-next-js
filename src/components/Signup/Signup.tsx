// Core
import { memo, useState, useCallback, useMemo } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { setCookie, destroyCookie } from 'nookies';
import { Form, Field } from 'react-final-form';
import { useTranslation } from 'next-i18next';

// API
import { IErrorFetch, apiSignup } from '@/lib/api';

// Types
import { ISignup } from '@/types';

interface ISignupForm extends ISignup {
    passwordRepeat: string;
}

const subscriptionForm = {};

const subscriptionField = {
    value: true,
};

interface IAlert {
    type: string;
    message: string;
}

const Signup = () => {
    const [alert, setAlert] = useState<IAlert | null>(null);

    const router = useRouter();
    const { t } = useTranslation();
    const translation =  useMemo(() => ({
        header:                          t('signup:header'),
        title:                           t('signup:title'),
        'placeholder-fullname':          t('signup:placeholder-fullname'),
        'placeholder-email':             t('signup:placeholder-email'),
        'placeholder-password':          t('signup:placeholder-password'),
        'placeholder-password-repeat':   t('signup:placeholder-password-repeat'),
        'button-submit':                 t('signup:button-submit'),
        'alert-success':                 t('signup:alert-success'),
        'validation-not-match-password': t('signup:validation-not-match-password'),
        hasAccount:                      t('signup:hasAccount'),
        'link-signin':                   t('signup:link-signin'),
        copyright:                       t('common:copyright'),
    }), []);

    const year = new Date().getFullYear();

    const validate = useCallback(
        ({ password, passwordRepeat }: Partial<ISignupForm>) => {
            const error: Partial<Record<keyof ISignupForm, string>> = {};
            if (password !== passwordRepeat) {
                const message = translation[ 'validation-not-match-password' ];
                error.passwordRepeat = message;
            }

            const lastError = Object.values(error).reverse()[ 0 ];
            if (lastError) {
                setAlert({ type: 'danger', message: lastError });
            } else {
                setAlert(null);
            }

            return error;
        },
        [],
    );

    const onSubmit = useCallback(
        async ({ passwordRepeat, ...rest }: ISignupForm) => {
            try {
                const { data }: { data: string }  = await apiSignup(rest);

                setAlert({ type: 'success', message: translation[ 'alert-success' ] });
                setCookie(null, 'JWT', data, { path: '/' });
                destroyCookie(null, 'profile');
                router.push('/');
            } catch (error) {
                const { message } = error as IErrorFetch;
                setAlert({ type: 'danger', message });
            }
        },
        [],
    );

    return (
        <div className = 'sign_in_up_bg'>
            <div className = 'container'>
                <div className = 'row justify-content-lg-center justify-content-md-center'>
                    <div className = 'col-lg-12'>
                        <div className = 'main_logo25' id = 'logo'>
                            <Link href = '/'>
                                <a>
                                    <Image
                                        src = '/images/logo.svg'
                                        layout = 'fill'
                                        alt = ''
                                    />
                                    <span className = 'logo-inverse'>
                                        <Image
                                            src = '/images/ct_logo.svg'
                                            layout = 'fill'
                                            alt = ''
                                        />
                                    </span>
                                </a>
                            </Link>
                        </div>
                    </div>

                    <div className = 'col-lg-6 col-md-8'>
                        <div className = 'sign_form'>
                            <h2>{ translation.header }</h2>
                            <p>{ translation.title }</p>
                            <Form
                                subscription = { subscriptionForm }
                                onSubmit = { onSubmit }
                                validate = { validate }
                                render = { ({ handleSubmit }) => (
                                    <form onSubmit = { handleSubmit }>
                                        <div className = 'ui search focus'>
                                            <div className = 'ui left icon input swdh11 swdh19'>
                                                <Field
                                                    subscription = { subscriptionField }
                                                    component = 'input'
                                                    type = 'text'
                                                    name = 'name'
                                                    id = 'id_fullname'
                                                    required
                                                    maxLength = { 64 }
                                                    className = 'prompt srch_explore'
                                                    placeholder = { translation[ 'placeholder-fullname' ] }
                                                />
                                            </div>
                                        </div>
                                        <div className = 'ui search focus mt-15'>
                                            <div className = 'ui left icon input swdh11 swdh19'>
                                                <Field
                                                    component = 'input'
                                                    type = 'email'
                                                    name = 'email'
                                                    id = 'id_email'
                                                    required
                                                    maxLength = { 64 }
                                                    className = 'prompt srch_explore'
                                                    placeholder = { translation[ 'placeholder-email' ] }
                                                />
                                            </div>
                                        </div>
                                        <div className = 'ui search focus mt-15'>
                                            <div className = 'ui left icon input swdh11 swdh19'>
                                                <Field
                                                    subscription = { subscriptionField }
                                                    component = 'input'
                                                    type = 'password'
                                                    name = 'password'
                                                    required
                                                    maxLength = { 64 }
                                                    className = 'prompt srch_explore'
                                                    placeholder = { translation[ 'placeholder-password' ] }
                                                />
                                            </div>
                                        </div>
                                        <div className = 'ui search focus mt-15'>
                                            <div className = 'ui left icon input swdh11 swdh19'>
                                                <Field
                                                    subscription = { subscriptionField }
                                                    component = 'input'
                                                    type = 'password'
                                                    name = 'passwordRepeat'
                                                    required
                                                    maxLength = { 64 }
                                                    className = 'prompt srch_explore'
                                                    placeholder = { translation[ 'placeholder-password-repeat' ] }
                                                />
                                            </div>
                                        </div>
                                        <button className = 'login-btn' type = 'submit'>{ translation[ 'button-submit' ] }</button>
                                    </form>
                                ) }
                            />
                            {alert && <div className = { `alert alert-${alert!.type} mt-15` } role = 'alert'>
                                {alert.message}
                            </div>}
                            <p className = 'mb-0 mt-30'>
                                { translation.hasAccount } <Link href = '/signin'><a>{ translation[ 'link-signin' ] }</a></Link>
                            </p>
                        </div>
                        <div className = 'sign_footer'>
                            © {year} <strong>Cursus</strong>. { translation.copyright }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(Signup);
