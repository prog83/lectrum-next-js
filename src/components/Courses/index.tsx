// Core
import { useQuery } from 'react-query';

// Components
import { CourseCard } from '../Course';

// API
import { IErrorFetch, apiCourses } from '@/lib/api';

// Types
import { ICourse } from '@/types';

export const Courses = () => {
    const { error, data: { data: courses = []} = {}} = useQuery<{ data: Array<ICourse> }, IErrorFetch>('courses', apiCourses, { staleTime: 5000 });

    if (error) {
        return  (
            <div className = 'alert alert-danger w-75 m-auto mt-15' role = 'alert'>
                {error.message}
            </div>);
    }

    return (
        <div className = 'section3125'>
            <div className = 'la5lo1'>
                <div className = 'featured_courses'>
                    { courses.map((course) => (<CourseCard key = { course.hash } { ...course } />)) }
                </div>
            </div>
        </div>
    );
};
