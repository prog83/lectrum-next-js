// Core
import { FC, memo } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

// Other
import { routes } from '../../routes';

interface IPropTypes {
    statusCode: number;
    title: string;
}

const CustomError: FC<IPropTypes> = ({ statusCode, title }) => {
    const { t } = useTranslation();
    const translation = {
        'link-home': t('404:link-home'),
        home:        t('404:home'),
        copyright:   t('common:copyright'),
    };

    return (
        <div className = 'wrapper coming_soon_style'>
            <div className = 'container'>
                <div className = 'row'>
                    <div className = 'col-md-12'>
                        <div className = 'cmtk_group'>
                            <div className = 'ct-logo'>
                                <Link href = { routes.homepage.path }>
                                    <a>
                                        <Image
                                            src = '/images/ct_logo.svg'
                                            alt = 'logo'
                                            layout = 'fixed'
                                            width = { 150 }
                                            height = { 35 }
                                        />
                                    </a>
                                </Link>
                            </div>
                            <div className = 'cmtk_dt'>
                                <h1 className = 'title_404'>{ statusCode }</h1>
                                <h4 className = 'thnk_title1'>{ title }</h4>
                                <div>
                                    <Link href = { routes.homepage.path }>
                                        <a className = 'bk_btn'>{ translation[ 'link-home' ] }</a>
                                    </Link>
                                </div>
                            </div>
                            <div className = 'tc_footer_main'>
                                <div className = 'tc_footer_left'>
                                    <ul>
                                        <li>
                                            <Link href = { routes.homepage.path }>
                                                <a>{ translation.home }</a>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                                <div className = 'tc_footer_right'>
                                    <p>
                                    &copy; 2021
                                        { ' ' }
                                        <Link href = { routes.homepage.path }>
                                            <a className = 'footer-link'>
                                                <strong>Lectrum LLC</strong>
                                            </a>
                                        </Link>
                                    .{' '}{ translation.copyright }
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(CustomError);
