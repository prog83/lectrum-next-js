// Core
import { useMemo } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

// Routes
import { routes } from '@/routes';

// Hooks
import { useProfile } from '@/hooks';

const ProfileCard = () => {
    const { t } = useTranslation('profile-card');
    const { name } = useProfile();

    const translation = useMemo(() => ({
        description:    t('description'),
        students:       t('students'),
        courses:        t('courses'),
        'link-profile': t('link-profile'),
    }), []);

    return name ? (
        <div className = 'fcrse_2 mb-30'>
            <div className = 'tutor_img'>
                <Link href = { routes.teacher.about.path }>
                    <a>
                        <Image
                            src = '/images/hd_dp.jpg'
                            alt = ''
                            layout = 'fixed'
                            width = { 100 }
                            height = { 100 }
                        />
                    </a>
                </Link>

            </div>
            <div className = 'tutor_content_dt'>
                <div className = 'tutor150'>
                    <Link href = { routes.teacher.about.path }>
                        <a className = 'tutor_name'>{ name }</a>
                    </Link>
                    <div className = 'mef78' title = 'Verify'>
                        <i className = 'uil uil-check-circle' />
                    </div>
                </div>
                <div className = 'tutor_cate'>{translation.description}</div>

                <div className = 'tut1250'>
                    <span className = 'vdt15'>{`615K ${translation.students}`}</span>
                    <span className = 'vdt15'>{`12 ${translation.courses}`}</span>
                </div>
                <Link href = { routes.teacher.about.path }>
                    <a className = 'prfle12link'>{translation[ 'link-profile' ]}</a>
                </Link>
            </div>
        </div>
    ) : null;
};

export default ProfileCard;
