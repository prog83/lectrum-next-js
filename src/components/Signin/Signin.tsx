// Core
import { memo, useState, useCallback, useMemo } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { setCookie, destroyCookie } from 'nookies';
import { Form, Field } from 'react-final-form';
import { useTranslation } from 'next-i18next';

// API
import { IErrorFetch, apiSignin } from '@/lib/api';

// Types
import { ISignin } from '@/types';

const subscriptionForm = {};

const subscriptionField = {
    value: true,
};

interface IAlert {
    type: string;
    message: string;
}

const Signin = () => {
    const [alert, setAlert] = useState<IAlert | null>(null);

    const router = useRouter();
    const { t } = useTranslation();
    const translation =  useMemo(() => ({
        header:                 t('signin:header'),
        title:                  t('signin:title'),
        'placeholder-email':    t('signin:placeholder-email'),
        'placeholder-password': t('signin:placeholder-password'),
        'button-submit':        t('signin:button-submit'),
        'alert-success':        t('signin:alert-success'),
        noAccount:              t('signin:noAccount'),
        'link-signup':          t('signin:link-signup'),
        copyright:              t('common:copyright'),
    }), []);

    const year = new Date().getFullYear();

    const onSubmit = useCallback(
        async (values: ISignin) => {
            try {
                const { data }: { data: string }  = await apiSignin(values);

                setAlert({ type: 'success', message: translation[ 'alert-success' ] });
                setCookie(null, 'JWT', data, { path: '/' });
                destroyCookie(null, 'profile');
                router.push('/');
            } catch (error) {
                const { message } = error as IErrorFetch;
                setAlert({ type: 'danger', message });
            }
        },
        [],
    );

    return (
        <div className = 'sign_in_up_bg'>
            <div className = 'container'>
                <div className = 'row justify-content-lg-center justify-content-md-center'>
                    <div className = 'col-lg-12'>
                        <div className = 'main_logo25' id = 'logo'>
                            <Link href = '/'>
                                <a>
                                    <Image
                                        src = '/images/logo.svg'
                                        layout = 'fill'
                                        alt = ''
                                    />
                                    <span className = 'logo-inverse'>
                                        <Image
                                            src = '/images/ct_logo.svg'
                                            layout = 'fill'
                                            alt = ''
                                        />
                                    </span>
                                </a>
                            </Link>
                        </div>
                    </div>

                    <div className = 'col-lg-6 col-md-8'>
                        <div className = 'sign_form'>
                            <h2>{ translation.header }</h2>
                            <p>{ translation.title }</p>
                            <Form
                                subscription = { subscriptionForm }
                                onSubmit = { onSubmit }
                                render = { ({ handleSubmit }) => (
                                    <form onSubmit = { handleSubmit }>
                                        <div className = 'ui search focus mt-15'>
                                            <div className = 'ui left icon input swdh95'>
                                                <Field
                                                    defaultValue = { process.env.NEXT_PUBLIC_EMAIL }
                                                    subscription = { subscriptionField }
                                                    name = 'email'
                                                    component = 'input'
                                                    type = 'email'
                                                    className = 'prompt srch_explore'
                                                    required
                                                    maxLength = { 64 }
                                                    placeholder =  { translation[ 'placeholder-email' ] }
                                                />
                                            </div>
                                        </div>
                                        <div className = 'ui search focus mt-15'>
                                            <div className = 'ui left icon input swdh95'>
                                                <Field
                                                    defaultValue = { process.env.NEXT_PUBLIC_PASSWORD }
                                                    subscription = { subscriptionField }
                                                    name = 'password'
                                                    component = 'input'
                                                    type = 'password'
                                                    className = 'prompt srch_explore'
                                                    required = { true }
                                                    maxLength = { 64 }
                                                    placeholder = { translation[ 'placeholder-password' ] }
                                                />
                                            </div>
                                        </div>
                                        <div className = 'ui form mt-30 checkbox_sign'>
                                            <div className = 'inline field'>
                                                <div className = 'ui checkbox mncheck'>
                                                    <input
                                                        type = 'checkbox'
                                                        tabIndex = { 0 }
                                                        className = 'hidden'
                                                        id = 'remember'
                                                    />
                                                    <label htmlFor = 'remember'>Remember Me</label>
                                                </div>
                                            </div>
                                        </div>
                                        <button className = 'login-btn' type = 'submit'>{ translation[ 'button-submit' ] }</button>
                                        {alert && <div className = { `alert alert-${alert!.type} mt-15` } role = 'alert'>
                                            {alert.message}
                                        </div>}
                                    </form>) }
                            />
                            <p className = 'mb-0 mt-30 hvsng145'>
                                { translation.noAccount } <Link href = '/signup'><a >{ translation[ 'link-signup' ] }</a></Link>
                            </p>
                        </div>
                        <div className = 'sign_footer'>
                            © {year} <strong>Lectrum LLC</strong>. {translation.copyright}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(Signin);
