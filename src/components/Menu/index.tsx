// Core
import { FC, useState, useEffect, useCallback, useMemo } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import router from 'next/router';
import { useTranslation } from 'next-i18next';
import { parseCookies, destroyCookie } from 'nookies';

// Elements
import Logo from '@/elements/Logo';

// Hooks
import { useProfile } from '@/hooks';

import { IErrorFetch, apiLogout } from '@/lib/api';

const Menu: FC = () => {
    const [alert, setAlert] = useState<string | null>(null);
    const [JWT, setJWT] = useState<string | undefined>();
    const { t } = useTranslation('menu');
    const { name } = useProfile();

    const translation = useMemo(() => ({
        account:       t('account'),
        button_upload: t('button_upload'),
        login:         t('login'),
        logout:        t('logout'),
    }), []);

    useEffect(() => {
        const cookies = parseCookies();
        setJWT(cookies.JWT);
    });


    const handleLogout =  useCallback(
        async () => {
            try {
                await apiLogout(JWT!);

                destroyCookie(null, 'JWT');
                destroyCookie(null, 'profile');
                router.push('/');
            } catch (error) {
                const { message } = error as IErrorFetch;
                setAlert(message);
            }
        },
        [JWT],
    );

    return (
        <header className = 'header clearfix'>
            <style jsx>
                {`
                    .header-alert {
                        position: absolute;
                        top: 50%;
                        left: 50%;
                        transform: translate(-50%, -50%);
                    }
                    .logout {
                        cursor: pointer;
                    }
                `}
            </style>
            {alert && <div className = 'alert alert-danger header-alert' role = 'alert'>
                {alert}
            </div>}
            <Logo />
            <div className = 'header_right'>
                <ul>
                    <li>
                        <a
                            href = '#'
                            className = 'upload_btn'
                            title = { translation.button_upload }
                        >
                            {translation.button_upload}
                        </a
                        >
                    </li>
                    <li className = 'ui dropdown'>
                        <Link href = '/teacher/about' >
                            <a
                                className = 'opts_account _df7852'
                                title = { translation.account }
                            >
                                {name}
                                <div className = 'user-icon'>
                                    <Image
                                        src = '/images/hd_dp.jpg'
                                        alt = ''
                                        layout = 'fill'
                                        objectFit = 'contain'
                                    />
                                </div>
                            </a>
                        </Link>
                    </li>
                    <li className = 'ui dropdown'>
                        {JWT ? (
                            <a
                                onClick = { handleLogout }
                                className = 'opts_account log_out _5f7g11 logout'
                                title = { translation.logout }
                            >
                                {translation.logout}
                            </a>)
                            : (
                                <Link href = '/login'>
                                    <a className = 'opts_account log_out _5f7g11' title =  { translation.login }>
                                        {translation.login}
                                    </a>
                                </Link>)
                        }
                    </li>
                </ul>
            </div>
        </header>
    );
};

export default Menu;
