// Core
import { memo } from 'react';
import { parseCookies } from 'nookies';
import { useQuery } from 'react-query';
import { useTranslation } from 'next-i18next';

// Components
import { CourseCard } from '@/components/Course';

// API
import { IErrorFetch, apiTeacherCourses } from '@/lib/api';

// Types
import { ICourse } from '@/types';

// Other
import { getProfile } from '@/lib';

const Courses = () => {
    const { t } = useTranslation('teacher');
    const translation = {
        'tab-courses': t('tab-courses'),
    };

    const { JWT } = parseCookies();
    const { name = '' } = getProfile() ?? {};
    const { error, data: { data: courses = []} = {}} = useQuery<{ data: Array<ICourse> }, IErrorFetch>(['teacher-courses', name], () => apiTeacherCourses(JWT), { staleTime: 5000 });
    const count = courses.length;

    if (error) {
        return  (
            <div className = 'alert alert-danger w-75 m-auto mt-15' role = 'alert'>
                {error}
            </div>);
    }

    return (
        <div className = 'crse_content'>
            <h3>{`${translation[ 'tab-courses' ]} (${count})`}</h3>
            <div className = '_14d25'>
                <div className = 'row'>
                    <div className = 'col-12'>
                        <div className = 'section3125'>
                            <div className = 'la5lo1'>
                                <div className = 'featured_courses'>
                                    {courses.map((course) => <CourseCard key = { course.hash } { ...course } />)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(Courses);
