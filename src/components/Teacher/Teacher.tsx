// Core
import React, { memo, useMemo } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

// Routes
import { routes } from '@/routes';

// Hooks
import { useProfile } from '@/hooks';

// Other
import { useStore } from '@/lib';
interface IProps {
    children: React.ReactNode;
}

const Teacher = ({ children }: IProps) => {
    const { pathname } = useRouter();

    const { name } = useProfile();
    const { t } = useTranslation('teacher');
    const translation = useMemo(() => ({
        description:   t('description'),
        'tab-about':   t('tab-about'),
        'tab-courses': t('tab-courses'),
    }), []);
    const store = useStore();
    const { error } = store || {};

    if (error) {
        return  (
            <div className = 'alert alert-danger w-75 m-auto mt-15' role = 'alert'>
                { error }
            </div>);
    }

    return (
        <div className = 'wrapper _bg4586'>
            <div className = '_216b01'>
                <div className = 'container-fluid'>
                    <div className = 'row justify-content-md-center'>
                        <div className = 'col-md-10'>
                            <div className = 'section3125 rpt145'>
                                <div className = 'row'>
                                    <div className = 'col-lg-7'>
                                        <div className = 'dp_dt150'>
                                            <div className = 'img148'>
                                                <div className = 'teacher-icon'>
                                                    <Image
                                                        src = '/images/hd_dp.jpg'
                                                        alt = ''
                                                        layout = 'fill'
                                                        objectFit = 'contain'
                                                    />
                                                </div>
                                            </div>
                                            <div className = 'prfledt1'>
                                                <h2>{ name }</h2>
                                                <span>{ translation.description }</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className = '_215b15'>
                <div className = 'container-fluid'>
                    <div className = 'row'>
                        <div className = 'col-lg-12'>
                            <div className = 'course_tabs'>
                                <nav>
                                    <div
                                        className = 'nav nav-tabs tab_crse'
                                        id = 'nav-tab'
                                        role = 'tablist'
                                    >
                                        <Link href =  { routes.teacher.about.path }>
                                            <a
                                                className = { `nav-item nav-link${
                                                    pathname === routes.teacher.about.path ? ' active' : ''
                                                }` }
                                                data-toggle = 'tab'
                                                role = 'tab'
                                                aria-selected = 'true'
                                            >
                                                { translation[ 'tab-about' ] }
                                            </a>
                                        </Link>
                                        <Link href = { routes.teacher.courses.path }>
                                            <a
                                                className = { `nav-item nav-link${
                                                    pathname === routes.teacher.courses.path ? ' active' : ''
                                                }` }
                                                data-toggle = 'tab'
                                                role = 'tab'
                                                aria-selected = 'false'
                                            >
                                                { translation[ 'tab-courses' ] }
                                            </a>
                                        </Link>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className = '_215b17'>
                <div className = 'container-fluid'>
                    <div className = 'row'>
                        <div className = 'col-lg-12'>
                            <div className = 'course_tab_content'>
                                <div className = 'tab-content' id = 'nav-tabContent'>
                                    <div
                                        className = 'tab-pane fade show active'
                                        // id="nav-about"
                                        role = 'tabpanel'
                                    >
                                        {children}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(Teacher);
