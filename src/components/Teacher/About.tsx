import { memo } from 'react';
import { useTranslation } from 'next-i18next';

const About = () => {
    const { t } = useTranslation('teacher');
    const translation = {
        'aboutMe-title':       t('aboutMe-title'),
        'aboutMe-description': t('aboutMe-description'),
    };

    return (
        <div className = '_htg451'>
            <div className = '_htg452'>
                <h3>{ translation[ 'aboutMe-title' ] }</h3>
                <p>
                    { translation[ 'aboutMe-description' ] }
                </p>
            </div>
        </div>
    );
};

export default memo(About);
