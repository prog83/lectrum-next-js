// Core
import { memo } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';
import { useTranslation } from 'next-i18next';
import { format } from 'date-fns';

// API
import { IErrorFetch, apiCourse } from '@/lib/api';

// Types
import { ICourse } from '@/types';

const Course = () => {
    const { query: { hash }} = useRouter();
    const { t } = useTranslation('course');

    const { error, data: { data: course } = {}} = useQuery<{ data: ICourse }, IErrorFetch>(['course', hash], () => apiCourse(hash as string), { staleTime: 5000 });

    if (error) {
        return  (
            <div className = 'alert alert-danger w-75 m-auto mt-15' role = 'alert'>
                {error}
            </div>);
    }

    const {
        badge,
        poster,
        rating,
        votes,
        views,
        description,
        info: {
            requirements = [],
            descriptions = [],
            benefits = [],
            descriptionSummary = [],
        } = {},
        createdBy = 'Noname',
        created = '',
    } = course || {};
    const translation = {
        badge:            t('badge'),
        ratings:          t('ratings'),
        views:            t('views'),
        subDescription:   t('subDescription'),
        studentsEnrolled: t('studentsEnrolled'),
        lang:             t('lang'),
        lastUpdated:      t('lastUpdated'),
        requirements:     t('requirements'),
        descriptions:     t('descriptions'),
        benefits:         t('benefits'),
    };
    const lastUpdated = Date.parse(created) ? `${translation.lastUpdated} ${format(new Date(created), 'MM/yyyy')}` : '';
    const requirementsList = requirements.map((requirement, index)=>(<li key = { `${index}-${requirement}` }>
        <span className = '_5f7g11'>
            {requirement}
        </span>
    </li>));
    const descriptionsList = descriptions.map((desc, index)=>(<li key = { `${index}-${desc}` }>
        <span className = '_5f7g11'>
            { desc }
        </span>
    </li>));
    const benefitsList = benefits.map((benefit, index)=>(<li key = { `${index}-${benefit}` }>
        <span className = '_5f7g11'><strong>{benefit}</strong></span>
    </li>));

    return (
        <div className = 'wrapper _bg4586'>
            <div className = '_215b01'>
                <div className = 'container-fluid'>
                    <div className = 'row'>
                        <div className = 'col-lg-12'>
                            <div className = 'section3125'>
                                <div className = 'row justify-content-center'>
                                    <div className = 'col-xl-4 col-lg-5 col-md-6'>
                                        <div className = 'preview_video'>
                                            <a
                                                href = '#'
                                                className = 'fcrse_img'
                                                data-toggle = 'modal'
                                                data-target = '#videoModal'
                                            >
                                                {poster && (
                                                    <Image
                                                        src = { poster }
                                                        alt = 'poster'
                                                        layout = 'fill'
                                                        objectFit = 'cover'
                                                    />)
                                                }
                                                <div className = 'course-overlay'>
                                                    { badge && <div className = 'badge_seller'>{ translation.badge }</div>}
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div className = 'col-xl-8 col-lg-7 col-md-6'>
                                        <div className = '_215b03'>
                                            <h2>{ description }</h2>
                                            <span className = '_215b04'>
                                                { translation.subDescription }
                                            </span
                                            >
                                        </div>
                                        <div className = '_215b05'>
                                            <div className = 'crse_reviews mr-2'>
                                                <i className = 'uil uil-star' />
                                                {rating}
                                            </div>
                                            {`(${votes} ${translation.ratings})`}
                                        </div>
                                        <div className = '_215b05'>{ `114,521 ${translation.studentsEnrolled}` }</div>
                                        <div className = '_215b06'>
                                            <div className = '_215b07'>
                                                <span><i className = 'uil uil-comment' /></span>
                                                { translation.lang }
                                            </div>
                                        </div>
                                        <div className = '_215b05'>{ `${views} ${translation.views}` }</div>
                                        <div className = '_215b05'>{lastUpdated}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className = '_215b15 _byt1458'>
                <div className = 'container-fluid'>
                    <div className = 'row'>
                        <div className = 'col-lg-12'>
                            <div className = 'user_dt5'>
                                <div className = 'user_dt_left'>
                                    <div className = 'live_user_dt'>
                                        <div className = 'user_img5'>
                                            <Image
                                                src = '/images/hd_dp.jpg'
                                                alt = ''
                                                layout = 'fixed'
                                                height = { 50 }
                                                width = { 50 }
                                            />
                                        </div>
                                        <div className = 'user_cntnt'  >
                                            <p className = '_df7852'>{ createdBy }</p>
                                        </div>
                                    </div>
                                </div>
                                <div className = 'user_dt_right'>
                                    <ul>
                                        <li>
                                            <div className = 'lkcm152'>
                                                <Image
                                                    src = '/images/like.svg'
                                                    className = 'like-icon'
                                                    alt = ''
                                                    layout = 'fixed'
                                                    width = { 24 }
                                                    height = { 24 }

                                                />
                                                <span>100</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className = 'lkcm152'>
                                                <Image
                                                    src = '/images/dislike.svg'
                                                    className = 'like-icon'
                                                    alt = ''
                                                    layout = 'fixed'
                                                    width = { 24 }
                                                    height = { 24 }
                                                />
                                                <span>20</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className = '_215b17'>
                <div className = 'container-fluid'>
                    <div className = 'row'>
                        <div className = 'col-lg-12'>
                            <div className = 'course_tab_content'>
                                <div className = 'tab-content' id = 'nav-tabContent'>
                                    <div
                                        className = 'tab-pane fade show active'
                                        id = 'nav-about'
                                        role = 'tabpanel'
                                    >
                                        <div className = '_htg451'>
                                            <div className = '_htg452'>
                                                {requirementsList.length > 0 ? (
                                                    <>
                                                        <h3>{ translation.requirements }</h3>
                                                        <ul className = '_abc124'>
                                                            {requirementsList}
                                                        </ul>
                                                    </>
                                                ) : null}
                                            </div>
                                            <div className = '_htg452 mt-35'>
                                                {descriptionsList.length > 0 ? (
                                                    <>
                                                        <h3>{ translation.descriptions }</h3>
                                                        <ul className = '_abc124'>
                                                            {descriptionsList}
                                                        </ul>
                                                    </>
                                                ) : null}
                                                {descriptionSummary && <p>
                                                    {descriptionSummary}
                                                </p>}
                                                {benefitsList.length > 0 ? (
                                                    <>
                                                        <p>{ translation.benefits }</p>
                                                        <ul className = '_abc124'>
                                                            {benefitsList}
                                                        </ul>
                                                    </>
                                                ) : null}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(Course);
