// Core
import { memo } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import formatDistance from 'date-fns/formatDistance';

// Helpers
import { numberWithCommas } from '@/helpers';
import { getLocaleDateFns } from '@/lib';

// Types
import { ICourse } from '@/types';

interface IProps extends  ICourse {}

export const CourseCard = ({
    hash,  badge, poster, rating, views, price, duration, description, technologies, createdBy = 'Noname', created,
}: IProps) => {
    const { locale } = useRouter();
    const { t } = useTranslation('course');

    const localeDateFns = getLocaleDateFns(locale);
    const translation = {
        badge: t('badge'),
        hours: t('hours'),
        views: t('views'),
        by:    t('by'),
    };
    const href = `/course/${hash}`;
    // const daysAgo = Math.floor((Date.now() - Date.parse(created)) / 1000 / 60 / 60 / 24);
    // const createdAgo = daysAgo === 0 ? 'now' : `${daysAgo} day${daysAgo > 1 ? 's' : ''} ago`;
    const relativeDate = formatDistance(new Date(created), new Date(), { addSuffix: true, locale: localeDateFns });

    return (
        <div className = 'item'>
            <div className = 'fcrse_1 mb-20'>
                <Link href = { href }>
                    <a className = 'fcrse_img'>
                        <Image
                            src = { poster! }
                            alt = 'course image'
                            layout = 'fill'
                            objectFit = 'cover'
                        />
                        <div className = 'course-overlay'>
                            { badge && <div className = 'badge_seller'>{translation.badge}</div> }
                            <div className = 'crse_reviews'>
                                <i className = 'uil uil-star' />
                                { rating }
                            </div>
                            <div className = 'crse_timer'>{ `${duration} ${translation.hours}` }</div>
                        </div>
                    </a>
                </Link>
                <div className = 'fcrse_content'>
                    <div className = 'vdtodt'>
                        <span className = 'vdt14'>{ `${numberWithCommas(views)} ${translation.views}` }</span>
                        <span className = 'vdt14'>{ relativeDate }</span>
                    </div>
                    <Link href = { href }>
                        <a className = 'crse14s'>
                            { description }
                        </a>
                    </Link>
                    <a
                        href = '#'
                        className = 'crse-cate'
                    >
                        { technologies }
                    </a
                    >
                    <div className = 'auth1lnkprce'>
                        <p className = 'cr1fot'>
                            {translation.by ? `${translation.by} ` : ''}
                            <a href = '#'>{ createdBy }</a>
                        </p>
                        <div className = 'prce142'>{ `$${price}` }</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default memo(CourseCard);
