export const routes = {
    homepage: {
        path: '/',
        page: '/index',
    },
    teacher: {
        about: {
            path: '/teacher/about',
            page: '/teacher/about/index',
        },
        courses: {
            path: '/teacher/courses',
            page: '/teacher/courses/index',
        },
    },
};
