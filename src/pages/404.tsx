// Core
import { GetStaticPropsContext } from 'next';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Components
import CustomError from '../components/CustomError';

const Error404 = () => {
    const { t } = useTranslation('404');

    return (<>
        <Head>
            <title>
                { t('head-title') }
            </title>
        </Head>
        <CustomError
            statusCode = { 404 }
            title = { t('title') }
        />
    </>
    );
};

export const getStaticProps =  async  ({ locale = 'en' }: GetStaticPropsContext) => ({
    props: {
        ...await serverSideTranslations(locale, ['common', '404']),
    },
});

export default Error404;
