// Core
import { GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import nookies from 'nookies';
import { QueryClient } from 'react-query';
import { dehydrate } from 'react-query/hydration';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Views
import { AppView } from '@/views/AppView';

// Components
import Teacher, { Courses } from '@/components/Teacher';

// API
import { IErrorFetch, apiTeacherCourses, getProfile } from '@/lib';

const Page = () =>  {
    const { t } = useTranslation('teacher');

    return (
        <>
            <Head>
                <title>
                    { t('head-title-courses') }
                </title>
            </Head>
            <AppView>
                <Teacher>
                    <Courses />
                </Teacher>
            </AppView>
        </>
    );
};


export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    const { locale = 'en'  } = context;
    const queryClient = new QueryClient();

    try {
        const cookies = nookies.get(context);
        const { name = '' } = getProfile(cookies) ?? {};
        const data = await apiTeacherCourses(cookies.JWT);
        queryClient.setQueryData(['teacher-courses', name], data);
    } catch (error) {
        const { status } = error as IErrorFetch;
        if (status === 401) {
            return {
                redirect: {
                    destination: '/login',
                    permanent:   false,
                },
            };
        }
    }

    return {
        props: {
            dehydratedState: dehydrate(queryClient),
            ...await serverSideTranslations(locale, ['common', 'menu', 'teacher', 'course']),
        },
    };
};

export default Page;
