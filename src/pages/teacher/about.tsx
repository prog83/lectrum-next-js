// Core
import { GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import nookies from 'nookies';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Views
import { AppView } from '@/views/AppView';

// Components
import Teacher, { About } from '@/components/Teacher';

// API
import { IErrorFetch, apiAuth } from '@/lib/api';

const TeacherAbout = () =>  {
    const { t } = useTranslation('teacher');

    return (
        <>
            <Head>
                <title>
                    { t('head-title-about') }
                </title>
            </Head>
            <AppView>
                <Teacher>
                    <About />
                </Teacher>
            </AppView>
        </>
    );
};

export const getServerSideProps = async (context: GetServerSidePropsContext) => {
    const { locale = 'en' } = context;
    let defaultData = {};

    try {
        const { JWT } = nookies.get(context);
        await apiAuth(JWT);
    } catch (error) {
        const { status, message } = error as IErrorFetch;
        defaultData = { error: message };
        if (status === 401) {
            return {
                redirect: {
                    destination: '/login',
                    permanent:   false,
                },
            };
        }
    }

    return {
        props: {
            defaultData,
            ...await serverSideTranslations(locale, ['common', 'menu', 'teacher']),
        },
    };
};

export default TeacherAbout;
