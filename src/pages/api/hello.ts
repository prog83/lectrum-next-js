// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// Core
import { NextApiRequest, NextApiResponse } from 'next';

const handler = (_req: NextApiRequest, res: NextApiResponse) => {
    res.status(200).json({ name: 'John Doe' });
};

export default handler;
