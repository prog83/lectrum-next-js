// Core
import { NextApiRequest, NextApiResponse } from 'next';

import { data } from '../../data';

const courses = (_req: NextApiRequest, res: NextApiResponse) => {
    res.status(200).json(data);
};

export default courses;
