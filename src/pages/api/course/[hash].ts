// Core
import { NextApiRequest, NextApiResponse } from 'next';

// Other
import { data } from '../../../data';

const course = (req: NextApiRequest, res: NextApiResponse) => {
    const { hash } = req.query;

    const preparedData = data.filter((item) => item.hash === hash);

    res.status(200).json(preparedData);
};

export default course;
