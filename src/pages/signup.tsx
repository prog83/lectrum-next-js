// Core
import { GetStaticPropsContext } from 'next';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Components
import  Signup  from '@/components/Signup';

const Page = () => {
    const { t } = useTranslation('signup');

    return (
        <>
            <Head>
                <title>
                    { t('head-title') }
                </title>
            </Head>
            <Signup />
        </>
    );
};

export const getStaticProps =  async  ({ locale = 'en' }: GetStaticPropsContext) => ({
    props: {
        ...await serverSideTranslations(locale, ['common', 'signup']),
    },
});

export default Page;
