// Core
import { GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import { QueryClient } from 'react-query';
import { dehydrate } from 'react-query/hydration';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Views
import { AppView } from '@/views/AppView';

// Components
import { Course } from '@/components/Course';

// API
import { IErrorFetch, apiCourseView } from '@/lib/api';

// Types
import { ICourse } from '@/types';

const Page = () => {
    const { t } = useTranslation('common');

    return (
        <>
            <Head>
                <title>
                    {t('course-hash-head-title')}
                </title>
            </Head>
            <AppView>
                <Course />
            </AppView>
        </>
    );
};

export const getServerSideProps = async ({ query, locale = 'en' }: GetServerSidePropsContext) => {
    const { hash } = query;
    const queryClient = new QueryClient();

    try {
        const data = await apiCourseView(hash as string);
        queryClient.setQueryData<ICourse>(['course', hash], data);
    } catch (error) {
        const { status } = error as IErrorFetch;
        if (status === 404) {
            return {
                notFound: true,
            };
        }
    }

    return {
        props: {
            dehydratedState: dehydrate(queryClient),
            ...await serverSideTranslations(locale, ['common', 'menu', 'course']),
        },
    };
};

export default Page;
