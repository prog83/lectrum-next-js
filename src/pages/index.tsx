// Core
import { GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import { QueryClient } from 'react-query';
import { dehydrate } from 'react-query/hydration';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

// Views
import { AppView } from '../views/AppView';
import { CoursesView } from '../views/CoursesView';

// Components
import ProfileCard from '../components/ProfileCard';
import { Courses } from '../components/Courses';

// API
import { apiCourses } from '@/lib/api';

const Home = () => (
    <>
        <Head>
            <title>
                Lectrum Cursus
            </title>
        </Head>
        <AppView>
            <CoursesView
                courses = { <Courses /> }
                profile = { <ProfileCard /> }
            />
        </AppView>
    </>
);

export const getServerSideProps = async ({ locale = 'en' }: GetServerSidePropsContext)  => {
    const queryClient = new QueryClient();
    await queryClient.prefetchQuery('courses', apiCourses);

    return {
        props: {
            dehydratedState: dehydrate(queryClient),
            ...await serverSideTranslations(locale, ['common', 'menu',  'profile-card', 'course']),
        },
    };
};

export default Home;
