// Core
import { useMemo } from 'react';
import App, { AppContext, AppProps } from 'next/app';
import Head from 'next/head';
import { appWithTranslation } from 'next-i18next';
import { parseCookies, setCookie } from 'nookies';
import { Hydrate,  QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

// Components
import { ContextProvider } from '@/lib';

// API
import { apiProfile } from '@/lib/api';

// Types
import { IProfile } from '@/types';

// Styles
import '@/styles/style.css';
import '@/styles/bootstrap.min.css';

// Other
import nextI18nConfig from '../../next-i18next.config';


const MyApp = ({ Component, pageProps }: AppProps) => {
    const { defaultData, ...rest } = pageProps;
    const queryClient = useMemo(() => new QueryClient(), []);

    return (
        <>
            <Head>
                <link
                    rel = 'stylesheet' href = 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap'
                />
            </Head>
            <QueryClientProvider client = { queryClient }>
                <Hydrate state = { pageProps.dehydratedState }>
                    <ContextProvider data = { defaultData }>
                        <Component { ...rest } />
                    </ContextProvider>
                </Hydrate>
                <ReactQueryDevtools initialIsOpen = { false } />
            </QueryClientProvider>

        </>

    );
};

MyApp.getInitialProps = async (appContext: AppContext) => {
    try {
        const { ctx } = appContext;
        const { JWT, profile } = parseCookies(ctx);

        if (JWT && !profile) {
            const { data: { name, email }}: {data: IProfile}  = await apiProfile(JWT);

            setCookie(
                ctx,
                'profile',
                JSON.stringify({ name, email }),
                { path: '/' },
            );
        }
    } catch (error) {
        // console.log(error);
    }

    const appProps = await App.getInitialProps(appContext);

    return { ...appProps };
};

export default appWithTranslation(MyApp, nextI18nConfig);

