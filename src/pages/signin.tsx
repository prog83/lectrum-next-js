// Core
import { GetStaticPropsContext } from 'next';
import Head from 'next/head';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

// Components
import  Signin  from '@/components/Signin';

const Page = () => {
    const { t } = useTranslation('signin');

    return (
        <>
            <Head>
                <title>
                    { t('head-title') }
                </title>
            </Head>
            <Signin />
        </>
    );
};

export const getStaticProps =  async  ({ locale = 'en' }: GetStaticPropsContext) => ({
    props: {
        ...await serverSideTranslations(locale, ['common', 'signin']),
    },
});

export default Page;
