// Core
import { memo } from 'react';
import Link from 'next/link';
import Image from 'next/image';

// Other
import { routes } from '@/routes';

const Logo = () => (
    <div className = 'main_logo' id = 'logo'>
        <Link href = { routes.homepage.path }>
            <a>
                <Image
                    src = '/images/logo.svg'
                    layout = 'fill'
                    alt = ''
                />
                <span className = 'logo-inverse'>
                    <Image
                        src = '/images/ct_logo.svg'
                        layout = 'fill'
                        alt = ''
                    />
                </span>
            </a>
        </Link>
    </div>
);

export default memo(Logo);

