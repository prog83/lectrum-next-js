import { useState, useEffect } from 'react';

// Lib
import { getProfile } from '@/lib';

const useProfile = () => {
    const [name, setName] = useState<string | undefined>();
    const [email, setEmail] = useState<string | undefined>();

    useEffect(() => {
        const profile = getProfile();
        setName(profile?.name);
        setEmail(profile?.email);
    });


    return { name, email };
};

export default useProfile;
