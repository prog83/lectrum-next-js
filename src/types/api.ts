export interface IBadRequest {
    statusCode: number;
    message: string;
    error?: string;
}

export interface ISignin {
    email: string;
    password: string;
}

export interface ISignup {
    email: string;
    name: string;
    password: string;
}
export interface ICourse {
    hash: string;
    badge?: boolean;
    poster?: string;
    rating: number;
    votes: number;
    views: number;
    price: number;
    duration: number;
    description: string;
    technologies: string;
    createdBy?: string;
    created: string;
    info: {
        requirements: Array<string>;
        descriptions: Array<string>;
        benefits: Array<string>;
        descriptionSummary: string
    }
}

export interface IProfile {
    name: string;
    email: string;
}
