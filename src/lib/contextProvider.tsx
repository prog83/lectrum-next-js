import { createContext, useContext } from 'react';

interface IData {
    error?: string;
}
interface IProps {
    data: IData;
    children: React.ReactNode;
}

const Context = createContext<IData | null>(null);

export const useStore = () => useContext(Context);

const ContextProvider = ({ data, children }: IProps) => (
    <Context.Provider value = { data }>
        { children }
    </Context.Provider>
);

export default ContextProvider;
