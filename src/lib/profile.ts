// Core
import { parseCookies } from 'nookies';

// Types
import { IProfile } from '@/types';

export const getProfile = (cookies?: Record<string, string>) => {
    try {
        const { profile } = cookies || parseCookies();
        const {  name, email }:  IProfile =  JSON.parse(profile);

        return { name, email };
    } catch (error) {
        return null;
    }
};
