import { de, ru } from 'date-fns/locale';

export const getLocaleDateFns = (locale?: string) => {
    switch (locale) {
        case 'de':
            return de;

        case 'ru':
            return ru;

        default:
            break;
    }
};
