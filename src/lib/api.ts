// Types
import { IBadRequest, ISignin, ISignup } from '@/types';

export interface IErrorFetch {
    status: number;
    message: string;
}

const ErrorFetch = class extends Error implements IErrorFetch {
    readonly status: number;
    constructor(status: number, message: string) {
        super(message);
        this.status = status;
        Object.setPrototypeOf(this, ErrorFetch.prototype);
    }
};

export const checkErrorFetch = async (response: Response) => {
    const { status, statusText } = response;
    if (!response.ok) {
        const { message = statusText }: IBadRequest = await response.json();
        throw new ErrorFetch(status, message);
    }
};

export const apiSignin = async (data: ISignin) => {
    const response = await fetch(process.env.NEXT_PUBLIC_LECTRUM_SIGNIN!, {
        method:  'POST',
        headers: {
            accept:         'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    await checkErrorFetch(response);

    return response.json();
};

export const apiSignup = async (data: ISignup) => {
    const response = await fetch(process.env.NEXT_PUBLIC_LECTRUM_SIGNUP!, {
        method:  'POST',
        headers: {
            accept:         'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    await checkErrorFetch(response);

    return response.json();
};

export const apiAuth = async (token?: string) => {
    const response = await fetch(process.env.LECTRUM_AUTH!, {
        method:  'GET',
        headers: {
            accept:        '*/*',
            Authorization: `Bearer ${token}`,
        },
    });
    await checkErrorFetch(response);
};

export const apiLogout = async (token?: string) => {
    const response = await fetch(process.env.NEXT_PUBLIC_LECTRUM_LOGOUT!, {
        method:  'GET',
        headers: {
            accept:        '*/*',
            Authorization: `Bearer ${token}`,
        },
    });
    await checkErrorFetch(response);
};

export const apiProfile = async (token: string) =>  {
    const response = await fetch(process.env.LECTRUM_PROFILE!, {
        method:  'GET',
        headers: {
            accept:        'application/json',
            Authorization: `Bearer ${token}`,
        },
    });
    await checkErrorFetch(response);

    return response.json();
};

export const apiCourses = async () => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_LECTRUM_NEXTJS_COURSES_API}?page=1&limit=20`);
    await checkErrorFetch(response);

    return response.json();
};

export const apiTeacherCourses = async (token?: string) => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_LECTRUM_NEXTJS_TEACHER_COURSES_API}?page=1&limit=20`, {
        method:  'GET',
        headers: {
            accept:        'application/json',
            Authorization: `Bearer ${token}`,
        },
    });
    await checkErrorFetch(response);

    return  response.json();
};

export const apiCourse = async (id: string) => {
    const response =  await fetch(`${process.env.NEXT_PUBLIC_LECTRUM_NEXTJS_COURSES_API}/${id}`);
    await checkErrorFetch(response);

    return response.json();
};

export const apiCourseView = async (id: string) => {
    const response = await fetch(`${process.env.NEXT_PUBLIC_LECTRUM_NEXTJS_COURSES_API}/${id}/views`, {
        method: 'PUT',
    });
    await checkErrorFetch(response);

    return response.json();
};
