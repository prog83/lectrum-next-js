// Core
import { FC, ReactElement } from 'react';

// Components
import Menu from '../../components/Menu';
import { Footer } from '../../components/Footer';

export const AppView: FC<IPropTypes> = ({ children }) => (
    <>
        <Menu />
        <div className = 'wrapper'>
            <div className = 'sa4d25'>
                { children }
                <Footer />
            </div>
        </div>
    </>
);

interface IPropTypes {
    children: ReactElement | ReactElement[]
}
