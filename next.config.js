const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanCss = require('clean-css');
const { i18n } = require('./next-i18next.config');

/* eslint-disable */
module.exports = {
    reactStrictMode: true,
    poweredByHeader: false,
    basePath: process.env.NEXT_PUBLIC_BASE_PATH,
    images: {
        domains: ['lectrum.io', 'placeimg.com'],
    },

    async redirects() {
        return [
            {
            source: '/login',
            destination: '/signin',
            permanent: true,
            },
        ]
      },

    webpack: (config) => {
        const isProduction = process.env.NODE_ENV === 'production';

        if (isProduction) {
            config.optimization.minimizer.push(
                new OptimizeCssAssetsPlugin({
                    assetNameRegExp: /\.css$/g,
                    cssProcessor: CleanCss,
                    cssProcessorOptions: {
                        level: {
                            1: {
                                all: true,
                                normalizeUrls: false,
                            },
                            2: {
                                restructureRules: true,
                                removeUnusedAtRules: true,
                                skipProperties: ['border-top', 'border-bottom'],
                            },
                        },
                    },
                    canPrint: true,
                })
            );
        }

        return config;
    },

    i18n,
};
