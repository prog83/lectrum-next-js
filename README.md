# lectrum-next-js

Lectrum - Next.js

Vercel - [demo](https://lectrum-next-js.vercel.app)

## Installation

Download or git clone [source code](https://gitlab.com/prog83/lectrum-next-js).

Use the package manager [npm](https://www.npmjs.com/get-npm) to install.

```bash
npm i
```

## Usage

### Environment variables

NEXT_PUBLIC_BASE_PATH - base path app (set default '')

### Development

```bash
npm run dev
```

Ready on http://localhost:3000

### Production

```bash
npm run build
npm run start
```

Ready on http://localhost:3000

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
